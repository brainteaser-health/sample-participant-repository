# Dream Team for iDPP@CLEF 2024  #

This repository contains the runs, resources, and code, of the `Dream Team`, participating in the [iDPP@CLEF 2024](https://brainteaser.health/open-evaluation-challenges/idpp-2024/)
community effort. 

### Organisation of the repository ###

The repository is organised as follows:

* `submission`: this folder contains the runs submitted for the different tasks.
* `score`: this folder contains the performance scores of the submitted runs.
* `code`: this folder contains the source code of the developed system.
* `resource`: this folder contains any additional resources created during the participation;
* `report`: this folder contains the template for the participant report.

iDPP@CLEF 2024 consists of *three tasks* 

* **Task 1** - Predicting ALSFRS-R Score from Sensor Data (Amyotrophic Lateral Sclerosis)
* **Task 2** - Predicting Patient Self-assessment Score from Sensor Data (Amyotrophic Lateral Sclerosis)
* **Task 3** - Predicting Relapses from EDDS Sub-scores and Environmental Data (Multiple Sclerosis)

Therefore, the `submission` and `score` folders are organized into sub-folders for each task.


### License ###

All the contents of this repository are shared using the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/). 

![CC logo](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

